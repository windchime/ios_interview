//
//  DatabaseManager.h
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "QuestionRecord.h"

#define dataBasePath [[(NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES)) lastObject]stringByAppendingPathComponent:dataBaseName]
#define dataBaseName @"questionDataBase.sqlite"

@interface DatabaseManager : NSObject

+ (FMDatabase *)createDataBase;

+ (void)closeDataBase;

+ (void)deleteDataBase;

+ (BOOL)isTableExist:(NSString *)tableName;

+ (BOOL)createQuestionTable;

+ (BOOL)clearQuestionTable;

+ (BOOL) saveOrUpdataQuestion:(QuestionRecord *)question;

+ (QuestionRecord *)selectQuestionByIndex:(NSInteger)index;

+ (QuestionRecord *)selectQuestionRecordByQuestionId:(NSString *)questionId;

+ (BOOL)deleteQuestionRecordByQuestionId:(NSString *)questionId;

+ (NSInteger)getNumberOfQuestionsInDatabase;

+ (NSInteger)getMinIndexOfQuestionsInDatabase;

+ (NSArray *)getAllQuestionsInDatabase;

@end
