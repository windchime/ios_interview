//
//  PartnerLocation.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"

@protocol PartnerLocation @end
@interface PartnerLocation : JSONModel

@property (assign,nonatomic) double lat;
@property (assign,nonatomic) double lng;
@property (strong,nonatomic) NSNumber<Optional>* radius;

@end
