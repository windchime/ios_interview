//
//  ViewController.m
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import "ViewController.h"
#import "DatabaseManager.h"
#import "QuestionRecord.h"
#import "Question.h"

#define ViewWidth self.view.bounds.size.width

@interface ViewController () {
    int index;
}

@property (nonatomic,weak) UIButton *createQuestion;
@property (nonatomic,weak) UIButton *changeQuestion;
@property (nonatomic,weak) UIButton *deleteQuestion;
@property (nonatomic,weak) UIButton *clearQuestion;
@property (nonatomic,weak) UILabel *currentQuestionInfo;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    index = 0;
    [self initButtons];
    [self initLabel];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    index = [DatabaseManager getNumberOfQuestionsInDatabase];
    NSLog(@"index is %ld",index);
    [self refreshQuestionLabel];
}

- (void)initButtons {
    if (!_createQuestion) {
        UIButton *button1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ViewWidth*0.5, 44)];
        _createQuestion = button1;
        _createQuestion.center = self.view.center;
        _createQuestion.backgroundColor = [UIColor blueColor];
        [_createQuestion setTitle:@"CREATE QUESTION" forState:UIControlStateNormal];
        [_createQuestion addTarget:self action:@selector(create:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_createQuestion];
    }
    if (!_changeQuestion) {
        UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ViewWidth*0.5, 44)];
        _changeQuestion = button2;
        _changeQuestion.backgroundColor = [UIColor blueColor];
        _changeQuestion.center = CGPointMake(self.view.center.x, self.view.center.y+50);
        [_changeQuestion setTitle:@"CHANGE QUESTION" forState:UIControlStateNormal];
        [_changeQuestion addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_changeQuestion];
    }
    if (!_deleteQuestion) {
        UIButton *button3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ViewWidth*0.5, 44)];
        _deleteQuestion = button3;
        _deleteQuestion.backgroundColor = [UIColor blueColor];
        _deleteQuestion.center = CGPointMake(self.view.center.x, self.view.center.y+100);
        [_deleteQuestion setTitle:@"DELETE QUESTION" forState:UIControlStateNormal];
        [_deleteQuestion addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_deleteQuestion];
    }
    
    if (!_clearQuestion) {
        UIButton *button4 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ViewWidth*0.5, 44)];
        _clearQuestion = button4;
        _clearQuestion.backgroundColor = [UIColor blueColor];
        _clearQuestion.center = CGPointMake(self.view.center.x, self.view.center.y+150);
        [_clearQuestion setTitle:@"CLEAR" forState:UIControlStateNormal];
        [_clearQuestion addTarget:self action:@selector(clear:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_clearQuestion];
    }

}

- (void)initLabel {
    if (!_currentQuestionInfo) {
        UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 280)];
        _currentQuestionInfo = info;
        _currentQuestionInfo.text = @"init label";
        _currentQuestionInfo.numberOfLines = 0;
        _currentQuestionInfo.adjustsFontSizeToFitWidth = YES;
        _currentQuestionInfo.textColor = [UIColor lightGrayColor];
        _currentQuestionInfo.textAlignment = NSTextAlignmentCenter;
        _currentQuestionInfo.center = CGPointMake(self.view.center.x, self.view.center.y-200);
        [self.view addSubview:_currentQuestionInfo];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)create:(id)sender {
    index ++;
    Question *qe =[[Question alloc] init];
    qe.questionId = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    qe.name = @"abc";
    qe.count = [NSNumber numberWithInt:index];
    qe.isCorrect = [NSNumber numberWithBool:YES];
    QuestionRecord *record =[[QuestionRecord alloc] initWithQuestion:qe];
    [DatabaseManager saveOrUpdataQuestion:record];
    
    [self refreshQuestionLabel];
}

- (void)change:(id)sender {
    QuestionRecord *record =  [DatabaseManager selectQuestionByIndex:[DatabaseManager getMinIndexOfQuestionsInDatabase]];
    Question *qe = [[Question alloc] initWithString:record.questionStr error:nil];
    qe.name = @"ccc";
    QuestionRecord *newRecord = [[QuestionRecord alloc] initWithQuestion:qe];
    [DatabaseManager saveOrUpdataQuestion:newRecord];
    
    [self refreshQuestionLabel];
}

- (void)delete:(id)sender {
    QuestionRecord *record =  [DatabaseManager selectQuestionByIndex:[DatabaseManager getMinIndexOfQuestionsInDatabase]];
    NSLog(@"delete record :%@",record.questionId);
    [DatabaseManager deleteQuestionRecordByQuestionId:record.questionId];
    
    [self refreshQuestionLabel];
}

- (void)clear:(id)sender {
    [DatabaseManager clearQuestionTable];
    [self refreshQuestionLabel];
}

- (void)refreshQuestionLabel {
    NSString *str = @"";
    NSArray *allQuestions = [DatabaseManager getAllQuestionsInDatabase];
    for (int i = 0; i<allQuestions.count; i++) {
        QuestionRecord *qr = [allQuestions objectAtIndex:i];
        Question *qe = [[Question alloc] initWithString:qr.questionStr error:nil];
        str = [str stringByAppendingString:[NSString stringWithFormat:@"|%ld-%@-%lld-%@",qr.recordId,qe.name,[qe.count longLongValue],[qe.isCorrect boolValue]?@"YES":@"NO"]];
    }
    _currentQuestionInfo.text = str;
}


@end
