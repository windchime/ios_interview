//
//  DatabaseManager.m
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import "DatabaseManager.h"

#define TAG @"DatabaseManager"

@implementation DatabaseManager

static FMDatabase *shareDataBase = nil;


+ (FMDatabase *)createDataBase {
    @synchronized (self) {
        if (shareDataBase == nil) {
            shareDataBase = [FMDatabase databaseWithPath:dataBasePath];
        }
        return shareDataBase;
    }
}

+ (void)closeDataBase {
    if(![shareDataBase close]) {
        NSLog(@"%@: close database error", TAG);
        return;
    }
}

+ (void)deleteDataBase {
    if (shareDataBase != nil) {
        shareDataBase = nil;
    }
}

+ (BOOL) isTableExist:(NSString *)tableName {
    FMResultSet *rs = [shareDataBase executeQuery:@"select count(*) as 'count' from sqlite_master where type ='table' and name = ?", tableName];
    while ([rs next]) {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        NSLog(@"%@: %@ isOK %ld", TAG, tableName, (long) count);
        
        if (0 == count) {
            return NO;
        } else {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)createQuestionTable {
    shareDataBase = [DatabaseManager createDataBase];
    if ([shareDataBase open]) {
        if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]) {
            NSString *sql = @"CREATE TABLE local_interview_question_tabel (question_index INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL, question_id VARCHAR(100), question_str VARCHAR(500))";
            [shareDataBase executeUpdate:sql];
        }
        [shareDataBase close];
    }
    return YES;
}

+ (BOOL)clearQuestionTable {
    shareDataBase = [DatabaseManager createDataBase];
    if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]){
        if ([shareDataBase open]) {
            NSString * sql = @"delete from local_interview_question_tabel";
            BOOL res = [shareDataBase executeUpdate:sql];
            if (!res) {
                NSLog(@"%@: error to clearQuestionTable", TAG);
                [shareDataBase close];
                return NO;
            } else {
                NSLog(@"%@: succ to clearQuestionTable", TAG);
                [shareDataBase close];
                return YES;
            }
        }
    }
    return NO;
}

+ (BOOL) saveOrUpdataQuestion:(QuestionRecord *)question {
    NSLog(@"%@: saveOrUpdataQuestion %@",TAG,question.questionId);
    BOOL isOk = NO;
    BOOL hasSame;
    shareDataBase = [DatabaseManager createDataBase];
    if ([shareDataBase open]) {
        FMResultSet *s = [shareDataBase executeQuery:[NSString stringWithFormat:@"SELECT * FROM \"local_interview_question_tabel\" WHERE \"question_id\" = '%@'", question.questionId]];
        hasSame = [s next]?YES:NO;
        if (hasSame) {
            isOk = [shareDataBase executeUpdate:
                    [NSString stringWithFormat:@"UPDATE local_interview_question_tabel SET question_str = '%@' WHERE question_id = '%@'",question.questionStr,question.questionId]];
            NSLog(@"update :%@",isOk?@"suc":@"failed");
        } else {
            isOk = [shareDataBase executeUpdate:
                    @"INSERT INTO \"local_interview_question_tabel\" (\"question_id\",\"question_str\") VALUES(?,?)",question.questionId,question.questionStr];
            NSLog(@"INSERT :%@",isOk?@"suc":@"failed");
        }
        [shareDataBase close];
    }
    return isOk;
}

+ (QuestionRecord *)selectQuestionByIndex:(NSInteger)index {
    shareDataBase = [DatabaseManager createDataBase];
    QuestionRecord *question = nil;
    if ([shareDataBase open]) {
        FMResultSet *s = [shareDataBase executeQuery:[NSString stringWithFormat:@"SELECT * FROM \"local_interview_question_tabel\" WHERE \"question_index\" = '%d'",index]];
        if ([s next]) {
            question = [[QuestionRecord alloc] init];
            question.questionStr = [s stringForColumn:@"question_str"];
            question.recordId = [s intForColumn:@"question_index"];
            question.questionId = [s stringForColumn:@"question_id"];
        }
        [shareDataBase close];
    }
    [shareDataBase close];
    
    return question;
}

+ (QuestionRecord *)selectQuestionRecordByQuestionId:(NSString *)questionId {
    QuestionRecord *question = nil;
    shareDataBase = [DatabaseManager createDataBase];
    if ([shareDataBase open]) {
        FMResultSet *s = [shareDataBase executeQuery:[NSString stringWithFormat:@"SELECT * FROM \"local_interview_question_tabel\" WHERE \"question_id\" = '%@'",questionId]];
        if ([s next]) {
            question = [[QuestionRecord alloc] init];
            question.questionStr = [s stringForColumn:@"question_str"];
            question.recordId = [s intForColumn:@"question_index"];
            question.questionId = [s stringForColumn:@"question_id"];
        }
        [shareDataBase close];
    }
    [shareDataBase close];
    
    return question;
}

+ (BOOL)deleteQuestionRecordByQuestionId:(NSString *)questionId {
    shareDataBase = [DatabaseManager createDataBase];
    if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]){
        if ([shareDataBase open]) {
            NSString * sql = [NSString stringWithFormat:@"delete from local_interview_question_tabel where question_id = '%@'",questionId];
            BOOL res = [shareDataBase executeUpdate:sql];
            if (!res) {
                NSLog(@"%@: error to delete question", TAG);
                [shareDataBase close];
                return NO;
            } else {
                NSLog(@"%@: succ to delete question", TAG);
                [shareDataBase close];
                return YES;
            }
        }
    }
    
    return NO;
}

+ (NSInteger)getNumberOfQuestionsInDatabase {
    NSInteger count = 0;
    shareDataBase = [DatabaseManager createDataBase];
    if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]){
        if ([shareDataBase open]) {
            NSString * sql = [NSString stringWithFormat:@"select count(*) as question_count from local_interview_question_tabel"];
             NSLog(@"getNumberOfQuestionsInDatabase:%@",sql);
                FMResultSet *s1  = [shareDataBase executeQuery:sql];
            NSLog(@"getNumberOfQuestionsInDatabase:%d",s1.columnCount);
                while ([s1 next]) {
                    count = [s1 intForColumn:@"question_count"];
                    NSLog(@"%@,getNumberOfQuestionsInDatabase,%ld",TAG,(long)count);
                }
            [shareDataBase close];
        }
    }
    return count;
}

+ (NSInteger)getMinIndexOfQuestionsInDatabase {
    NSInteger count = 0;
    shareDataBase = [DatabaseManager createDataBase];
    if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]){
        if ([shareDataBase open]) {
            NSString * sql = [NSString stringWithFormat:@"select min(question_index) as question_min_index from local_interview_question_tabel"];
            NSLog(@"getMinIndexOfQuestionsInDatabase:%@",sql);
            FMResultSet *s1  = [shareDataBase executeQuery:sql];
            NSLog(@"getNumberOfQuestionsInDatabase:%d",s1.columnCount);
            while ([s1 next]) {
                count = [s1 intForColumn:@"question_min_index"];
                NSLog(@"%@,getMinIndexOfQuestionsInDatabase,%ld",TAG,(long)count);
            }
            [shareDataBase close];
        }
    }
    return count;
}

+ (NSArray *)getAllQuestionsInDatabase {
    NSMutableArray *result = [NSMutableArray array];
    shareDataBase = [DatabaseManager createDataBase];
    if (![DatabaseManager isTableExist:@"local_interview_question_tabel"]){
        if ([shareDataBase open]) {
            NSString * sql = [NSString stringWithFormat:@"select *  from local_interview_question_tabel"];
            FMResultSet *s  = [shareDataBase executeQuery:sql];
            while ([s next]) {
                QuestionRecord *question = [[QuestionRecord alloc] init];
                question.questionStr = [s stringForColumn:@"question_str"];
                question.recordId = [s intForColumn:@"question_index"];
                question.questionId = [s stringForColumn:@"question_id"];
                [result addObject:question];
            }
            [shareDataBase close];
        }
    }
    return result;
}



@end
