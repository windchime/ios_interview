//
//  UserStatistic.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"

@interface UserStatistic : JSONModel


@property (strong,nonatomic) NSString<Optional>* profImgUrl;
@property (strong,nonatomic) NSString<Optional>* bragging;
@property (strong,nonatomic) NSString<Optional>* experience;
@property (strong,nonatomic) NSString<Optional>* selfDescription;
@property (strong,nonatomic) NSString<Optional>* facebookId;
@property (strong,nonatomic) NSNumber<Optional>* avgRatingAuthoredByCount;
@property (strong,nonatomic) NSNumber<Optional>* avgRatingAuthoredByValue;
@property (strong,nonatomic) NSNumber<Optional>* avgRatingReceivedByCount;
@property (strong,nonatomic) NSNumber<Optional>* avgRatingReceivedByValue;
@property (strong,nonatomic) NSNumber<Optional>* cancelledPurchaseCount;
@property (strong,nonatomic) NSNumber<Optional>* cancelledSaleCount;
@property (strong,nonatomic) NSNumber<Optional>* closedPurchaseCount;
@property (strong,nonatomic) NSNumber<Optional>* closedSaleCount;
@property (strong,nonatomic) NSNumber<Optional>* inProgressPurchaseCount;
@property (strong,nonatomic) NSNumber<Optional>* inProgressSaleCount;
@property (strong,nonatomic) NSNumber<Optional>* likeCount;
@property (strong,nonatomic) NSNumber<Optional>* nonActiveAdCount;
@property (strong,nonatomic) NSNumber<Optional>* soldAdCount;
@property (strong,nonatomic) NSNumber<Optional>* activeAdCount;
@property (strong,nonatomic) NSNumber<Optional>* closedPurchaseValue;
@property (strong,nonatomic) NSNumber<Optional>* requestCount;
@property (strong,nonatomic) NSNumber<Optional>* reviewsAuthoredCount;
@property (strong,nonatomic) NSNumber<Optional>* reviewsAuthoredValueInCents;
@property (strong,nonatomic) NSNumber<Optional>* reviewsReceivedCount;
@property (strong,nonatomic) NSNumber<Optional>* reviewsReceivedValueInCents;
@property (strong,nonatomic) NSNumber<Optional>* closedSaleValue;
@property (strong,nonatomic) NSNumber<Optional>* avgInitResponseTimeInSeconds;
@property (strong,nonatomic) NSNumber<Optional>* totalPoints;
@property (strong,nonatomic) NSNumber<Optional>* userLevel;
@property (strong,nonatomic) NSNumber<Optional>* pointsForNextLevel;

@end
