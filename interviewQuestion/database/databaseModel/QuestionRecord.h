//
//  QuestionRecord.h
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface QuestionRecord : NSObject

@property (assign, nonatomic) NSInteger recordId;
@property (strong, nonatomic) NSString* questionId;
@property (strong, nonatomic) NSString* questionStr;

- (id)initWithQuestion:(Question *)qe;

@end
