//
//  Funding.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"

//funding: {
//descriptor: "Blue Ladders",
//destination: MerchantAccount.FundingDestination.Bank,
//email: "funding@blueladders.com",
//mobilePhone: "5555555555",
//accountNumber: "1123581321",
//routingNumber: "071101307"
//}

@protocol Funding
@end

@interface Funding : JSONModel

@property (strong,nonatomic) NSNumber<Optional>* destinationID;
@property (strong,nonatomic) NSString<Optional>* descriptor;
@property (strong,nonatomic) NSString<Optional>* destination;
@property (strong,nonatomic) NSString<Optional>* email;
@property (strong,nonatomic) NSString<Optional>* mobilePhone;
@property (strong,nonatomic) NSString<Optional>* accountNumber;
@property (strong,nonatomic) NSString<Optional>* routingNumber;

@end
