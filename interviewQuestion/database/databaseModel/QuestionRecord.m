//
//  QuestionRecord.m
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import "QuestionRecord.h"

@implementation QuestionRecord

- (id)initWithQuestion:(Question *)qe {
    QuestionRecord * qr = [[QuestionRecord alloc] init];
    qr.questionId = [qe questionId];
    qr.questionStr = [qe toJSONString];
    return qr;
}

@end
