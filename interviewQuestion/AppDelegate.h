//
//  AppDelegate.h
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

