//
//  main.m
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
