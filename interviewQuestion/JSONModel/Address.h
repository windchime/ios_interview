//
//  Address.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"

//address: {
//streetAddress: "111 Main St",
//locality: "Chicago",
//region: "IL",
//postalCode: "60622"
//}
@protocol Address @end

@interface Address : JSONModel

@property (strong,nonatomic) NSString<Optional>* streetAddress;
@property (strong,nonatomic) NSString<Optional>* addressLine1;
@property (strong,nonatomic) NSString<Optional>* addressLine2;
@property (strong,nonatomic) NSString<Optional>* locality;
@property (strong,nonatomic) NSString<Optional>* region;
@property (strong,nonatomic) NSString<Optional>* postalCode;
@property (strong,nonatomic) NSString<Optional>* county;

@end
