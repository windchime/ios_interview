//
//  Business.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"
#import "Address.h"
//business: {
//legalName: "Jane's Ladders",
//dbaName: "Jane's Ladders",
//taxId: "98-7654321",
//address: {
//streetAddress: "111 Main St",
//locality: "Chicago",
//region: "IL",
//postalCode: "60622"
//}
//}

@protocol Business @end

@interface Business : JSONModel
@property (strong,nonatomic) NSNumber<Optional>* sellAsBusiness;
@property (strong,nonatomic) NSString<Optional>* legalName;
@property (strong,nonatomic) NSString<Optional>* dbaName;
@property (strong,nonatomic) NSString<Optional>* taxId;
@property (strong,nonatomic) Address<Optional>* address;

@end
