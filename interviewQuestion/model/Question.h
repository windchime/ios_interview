//
//  Question.h
//  interviewQuestion
//
//  Created by zeroy on 2018/10/2.
//  Copyright © 2018 zeroy. All rights reserved.
//

#import "JSONModel.h"

@interface Question : JSONModel

@property (strong, nonatomic) NSString<Optional>* questionId;
@property (strong, nonatomic) NSString<Optional>* name;
@property (strong, nonatomic) NSNumber<Optional>* count;
@property (strong, nonatomic) NSNumber<Optional>* isCorrect;

@end
