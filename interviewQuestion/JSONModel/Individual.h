//
//  Individual.h
//  Moomu
//
//  Copyright (c) 2015 Airahm Inc. All rights reserved.
//

#import "JSONModel.h"
#import "Address.h"
//individual: {
//firstName: "Jane",
//lastName: "Doe",
//email: "jane@14ladders.com",
//phone: "5553334444",
//dateOfBirth: "1981-11-19",
//ssn: "456-45-4567",
//address: {
//streetAddress: "111 Main St",
//locality: "Chicago",
//region: "IL",
//postalCode: "60622"
//}
@protocol Individual @end

@interface Individual : JSONModel

@property (strong,nonatomic) NSString<Optional>* firstName;
@property (strong,nonatomic) NSString<Optional>* lastName;
@property (strong,nonatomic) NSString<Optional>* phone;
@property (strong,nonatomic) NSString<Optional>* dateOfBirth;
@property (strong,nonatomic) NSString<Optional>* ssn;
@property (strong,nonatomic) Address<Optional>* address;

@end
